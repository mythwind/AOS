package cn.osworks.aos.modules.context;

import org.springframework.stereotype.Service;

/**
 * <b>提供一些平台级基础服务</b>
 * 
 * @author OSWorks-XC
 * @date 2014-05-13
 */
@Service("aosService")
public class AOSService {
		
}
